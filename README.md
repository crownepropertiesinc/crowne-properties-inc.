We buy houses on Oahu, Hawaii in cash, in any condition.
Hi, were Crowne Properties, home investors who will pay cash for your house. We buy houses anywhere in Oahu Hawaii, so if youre trying to sell your house fast, we can help. 

Why sell to us?
There are no fees and no commissions. Unlike with a real estate agent, you wont have to pay any up-front fees, closing costs, or any fees throughout the process. Theres no commission to pay either, youll just get paid the cash offer we make.
Well pay your closing costs for you.
There are no realtor fees to pay, and no commissions.
Well buy your property in as quickly as 7 days. We can also delay the closing until youre ready. So if you need a really quick sale, well work hard to make that happen, or wait until youre ready. Its your sale, and your decision.
Well buy your home totally as-is, which means you dont need to make repairs, improvements, or fix anything up. Even if your home is really ugly, in need of repair, or if a realtor cant list it because of the shape its in, we can help! Well also handle all the paperwork with the bank.

Website: https://www.sellmyhouseonoahu.com
